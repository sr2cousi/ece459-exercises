use std::sync::mpsc;
use std::thread;
use std::time::Duration;

static N: i32 = 3; // number of threads to spawn

fn main() {
    let mut children = vec![];
    let (tx, rx) = mpsc::channel();

    for i in 0..N {
        let child_tx = mpsc::Sender::clone(&tx);
        let child = thread::spawn(move || {
            child_tx.send(std::format!("Hello, {}!", i)).unwrap();
            thread::sleep(Duration::from_secs(1));
        });
        children.push(child);
    }
    while let Some(child) = children.pop() {
        child.join().unwrap();
    }

    for _ in 0..N {
        let received = rx.recv().unwrap();
        println!("Got: {}", received);
    }
}
