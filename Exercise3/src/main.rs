use std::thread;

static N: i32 = 10; // number of threads to spawn

fn main() {
    let mut children = vec![];

    // Create N children threads
    for i in 0..N {
        let child = thread::spawn(move || {
            println!("{}", i);
        });
        children.push(child);
    }

    // Join children threads
    while let Some(child) = children.pop() {
        child.join().unwrap();
    }
}
